#
#  Copyright (c) 2022. AnyKeyShik Rarity
#
#  admin@anykeyshik.xyz
#
#  https://t.me/AnyKeyShik
#

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, FileField, TextAreaField, PasswordField, URLField, SelectField, BooleanField


class ProjectForm(FlaskForm):
    project_name = StringField('Project title')
    project_url = URLField('Project URL')
    project_kind = StringField('Project kind (such as low-level, web, etc)')
    project_status = SelectField('Project status', choices=[('active', 'Active'), ('finished', 'Finished'), ('waiting', 'Paused'), ('dropped', 'Dropped')])
    project_description = TextAreaField('Project description')
    project_img_url = URLField('Img URL')
    project_img_file = FileField()
    submit = SubmitField("Save project")


class LoginForm(FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    remember_me = BooleanField("Remember me", default=True)
    submit = SubmitField('Login')
