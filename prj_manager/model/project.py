#
#  Copyright (c) 2022. AnyKeyShik Rarity
#
#  admin@anykeyshik.xyz
#
#  https://t.me/AnyKeyShik
#

from .. import db


class Project(db.Model):
    __tablename__ = 'projects'

    id = db.Column(db.Integer, primary_key=True)  # primary keys are required by SQLAlchemy
    name = db.Column(db.String(1000))
    url = db.Column(db.String(5000))
    kind = db.Column(db.String(1000))
    status = db.Column(db.String(100))
    status_css = db.Column(db.String(100))
    description = db.Column(db.String(5000))
    img_file = db.Column(db.String(5000))
    img_url = db.Column(db.String(5000))
