#
#  Copyright (c) 2022. AnyKeyShik Rarity
#
#  admin@anykeyshik.xyz
#
#  https://t.me/AnyKeyShik
#


import os

from flask import Blueprint, render_template, flash, request, make_response, redirect, url_for, \
    current_app, send_from_directory
from flask_login import login_required

from . import db
from .model import ProjectForm
from .model import Project

main = Blueprint('main', __name__)


@main.route('/', methods=['GET'])
def index():
    projects = Project.query.all()

    return render_template('index.html', projects=projects)


@main.route('/admin', methods=['GET'])
@login_required
def admin():
    projects = Project.query

    return render_template('admin.html', projects=projects)


@main.route('/edit', methods=['GET', 'POST'])
@login_required
def edit():
    id = request.args.get('id')
    form = ProjectForm()
    exists = False

    user_status = {
            'active': 'Active',
            'waiting': 'Paused',
            'finished': 'Finished',
            'dropped': 'Dropped'
            }

    project = Project.query.filter_by(id=id).first()
    if project:
        form.project_name.data = project.name
        form.project_url.data = project.url
        form.project_kind.data = project.kind
        form.project_status.data = project.status_css
        form.project_description.data = project.description
        
        if project.img_url:
            form.project_img_url.data = project.img_url
        else:
            form.project_img_file.data = project.img_file

        exists = True

    if request.method == 'POST' and form.validate():
        form = ProjectForm()

        filename = None
        if form.project_img_file.data:
            filename = os.path.join(current_app.config['UPLOAD_FOLDER'], form.project_img_file.data.filename)
            form.project_img_file.data.save(os.path.join(current_app.root_path, filename))

        if not exists:
            project = Project(name=form.project_name.data,
                              url=form.project_url.data, 
                              kind=form.project_kind.data,
                              status=user_status[form.project_status.data],
                              status_css=form.project_status.data,
                              description=form.project_description.data,
                              img_url=form.project_img_url.data,
                              img_file=filename
                            )
            db.session.add(project)
        else:
            Project.query.filter_by(id=id).update({
                'name': form.project_name.data,
                'kind': form.project_kind.data,
                'status': user_status[form.project_status.data],
                'status_css': form.project_status.data,
                'description': form.project_description.data,
                'img_url': form.project_img_url.data,
                'img_file': filename
                })
        db.session.commit()

        flash("Changes were successfully saved", 'success')
        return redirect(url_for('main.admin'))

    return render_template('edit_page.html', form=form, project=project)


@main.route('/delete', methods=['GET'])
@login_required
def delete():
    id = request.args.get('id')
    project = Project.query.filter_by(id=id).first()

    if project:
        db.session.delete(project)
        db.session.commit()
        flash("Project was successfully delete", 'success')
    else:
        flash("Can't delete empty project!", 'danger')

    return redirect(url_for('main.admin'))


@main.route('/uploads/<name>', methods=['GET'])
def download_file(name):
    return send_from_directory(current_app.config["UPLOAD_FOLDER"], name)
