#
#  Copyright (c) 2022. AnyKeyShik Rarity
#
#  admin@anykeyshik.xyz
#
#  https://t.me/AnyKeyShik
#

from secrets import token_hex

from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect

from prj_manager.consts import UPLOAD_FOLDER, DB_PATH

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)
    csrf = CSRFProtect()

    app.config['SECRET_KEY'] = token_hex(16)
    app.config['SQLALCHEMY_DATABASE_URI'] = DB_PATH
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

    app.add_url_rule(
        "/uploads/<name>", endpoint="download_file", build_only=True
    )

    db.init_app(app)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    csrf.init_app(app)

    from .model import User

    @login_manager.user_loader
    def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it in the query for the user
        return User.query.get(int(user_id))

    # blueprint for auth routes in our app
    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    # blueprint for non-auth parts of app
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app
