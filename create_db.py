#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from prj_manager import db, create_app, model
from passlib.hash import sha256_crypt
import argparse


def create():
    app = create_app()
    with app.app_context():
        db.create_all()

    return app


def add_admin(app, filename):
    with open(filename, 'r') as au:
        username = au.readline().rstrip('\n')
        password = sha256_crypt.hash(au.readline().rstrip('\n'))
        name = au.readline().rstrip('\n')
    
    admin_user = model.User(id=1, username=username,
                            password=password, name=name)

    with app.app_context():
        db.session.add(admin_user)
        db.session.commit()


def main():
    parser = argparse.ArgumentParser(description='Init database for project page')
    parser.add_argument('--create', action='store_true', help='Create DB for task page')
    parser.add_argument('--admin', action='store', help='Create admin user with credential from <file>')

    args = parser.parse_args()

    if args.create:
        app = create()
    else:
        app = create_app()

    if args.admin:
        add_admin(app, args.admin)


if __name__ == "__main__":
    main()
